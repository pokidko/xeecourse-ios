//
//  MainScreenViewController.swift
//  Xee Course
//
//  Created by Sergey Kobzin on 15.01.17.
//  Copyright © 2017 Sergey.Kobzin. All rights reserved.
//

import UIKit

class MainScreenViewController: UIViewController {
    
    @IBAction func buttonPressed(_ sender: Any) {
        (sender as! UIButton).backgroundColor = Constants.MAIN_RED
    }
    
    @IBAction func buttonReceived(_ sender: Any) {
        (sender as! UIButton).backgroundColor = Constants.LIGHT_GRAY
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
