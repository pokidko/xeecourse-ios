//
//  Constants.swift
//  Xee Course
//
//  Created by Sergey Kobzin on 15.01.17.
//  Copyright © 2017 Sergey.Kobzin. All rights reserved.
//

import UIKit

class Constants {
    
    static let MAIN_RED: UIColor = UIColor(red: 0xDD / 255, green: 0x2A / 255, blue: 0x1B / 255, alpha: 1)
    static let LIGHT_GRAY: UIColor = UIColor(red: 0xE6 / 255, green: 0xE6 / 255, blue: 0xE6 / 255, alpha: 1)

}
